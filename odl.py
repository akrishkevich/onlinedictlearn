from __future__ import division

__author__ = 'andrei.krishkevich'

#TODO: Write a dedicated exception class
#TODO: Provide option to turn off data validation (for speed-ups)
#TODO: Figure out how the `configure` method is going to work
#TODO: The `_data_is_batch` methods does redundant type validation if data validation is run
#TODO: Figure out how to incorporate convergence criteria into everything
#TODO: Perhaps matrix support should go altogether?
#TODO: Move hardcoded parameter initializations to the `configure` method

import numpy as np
from sklearn import linear_model
from scipy import misc
import matplotlib.pyplot as plt
import pylab
import os


class odl:
    """Online Dictionary Learning solver.

    Online Dictionary Learning (ODL) is an implementation of a paper by Julien Mairal and Francis Bach
    "Online Dictionary Learning for Sparse Coding" (http://www.di.ens.fr/willow/pdfs/icml09.pdf). This
    is an unsupervised learning algorithm used primarily to find sparse overcomplete representation for
    a corpus of data. Such representation can then be used as a preprocessing / feature extraction step
    for further classification, or as a basis for decompositions aimed at denoising.

    <attributes stub>
    If the class has public attributes, they should be documented here
    in an ``Attributes`` section and follow the same formatting as a
    function's ``Args`` section.

    Attributes:
      attr1 (str): Description of `attr1`.
      attr2 (list of str): Description of `attr2`.
      attr3 (int): Description of `attr3`.
    </attributes stub>

    """

    def __init__(self):
        """Initializes ODL solver.
        """

        # Setup model parameters
        self.model_is_initialized = False
        self.dim = 0
        self.dict_size = 0
        self.D = None
        self.A = None
        self.B = None
        self.alpha = None
        self.active_atoms = set()

        self.bcd_epsilon = 1e-10  # Dictionary update convergence parameter
        self.reg_lambda = 0.0008    # L1 norm regularization parameter

    def import_data(self, data):
        """Import data into ODC solver.

        WARNING: This function is going to be removed; part of initial deprecated API; violates "online" requirement.

        Data is expected to be either a matrix with samples corresponding to matrix columns. Imported data
        is automatically scaled and centered.

        Args:
          data (np.Matrix): Data to build dictionary for.

        """
        raise DeprecationWarning

    def configure(self):
        """
        Needs docstrings
        :return: nothing
        """
        raise NotImplementedError

    def process_new_data(self, new_data):
        """
        Updates dictionary model based on newly provided data (this is the "online" step).

        :param new_data: new data, column vector for single update, matrix for batch update
        :return: nothing
        """
        if not self._data_is_valid(new_data):
            raise Exception("Invalid data passed in")

        if not self.model_is_initialized:
            self._init_from_data(new_data)

        self._learn(new_data)

    def get_dictionary(self, max_count=None):
        """
        Returns dictionary currently learned by the model
        :param max_count: maximum number of dictionary atoms the return
        :return: dictionary
        """
        raise NotImplementedError

    @staticmethod
    def _data_is_batch(data):
        """
        Determines whether data is a single sample or a batch
        :param data: data
        :return: True is data is a batch, False otherwise
        """
        if len(data.shape) == 1:
            return False
        if data.shape[1] > 1:
            return True
        elif (data.shape[1] == 1) or (isinstance(data, np.array)):
            return False
        else:
            raise Exception("Data neither matrix nor array")

    def _data_is_valid(self, data):
        """
        Validates incoming data.

        Data must either be a numpy array or a matrix. Data samples must have the same dimension as the
        previous ones.

        :param data: data to validate
        :return: True is data is valid
        """
        is_array = type(data).__name__ == 'ndarray'
        is_matrix = type(data).__name__ == 'matrix'

        if not (is_array or is_matrix):
            return False

        size = data.size if is_array else data.size[0]

        if self.model_is_initialized:
            if not size == self.dim:
                return False
        else:
            if not size > 0:
                return False

        return True

    def _learn(self, new_data):
        """
        Updates model based on a new data (either single sample or batch).
        :param new_data: new data sample(s)
        :return: nothing
        """
        alpha = self._compute_sparse_atom_weights(new_data)
        self._compute_intermediate_state(new_data, alpha)
        self._update_dict(alpha)

    def _compute_sparse_atom_weights(self, data):
        """
        Compute sparse atom weight for `data` can be single sample or batch.
        :param data: new data sample(s)
        :return: nothing
        """
        clf = linear_model.LassoLars(alpha=self.reg_lambda, fit_path=False)
        clf.fit(self.D, data)
        return clf.coef_

    def _compute_intermediate_state(self, data, alpha):
        """
        Computes intermediate results needed for dictionary update from current sparse weights and data.
        :param data: current data (sample or batch)
        :param alpha: sparse weights for current data
        :return: nothing
        """
        if self._data_is_batch(data):
            raise NotImplementedError
        else:
            self.A += np.outer(alpha, alpha)
            self.B += np.outer(data, alpha)

    def _update_dict(self, alpha):
        """
        Updates dictionary based on new data using block-coordinate descent.

        As mentioned in [Mairal & Bach], we use the previously computed dictionary as a warm restart
        for the new dictionary calculation. The paper also says that empirically, a single update per
        atom is usually sufficient for convergence. For now, this remains to be seen.
        :param alpha: Sparse dictionary weights for latest data
        :return: nothing
        """
        max_steps = 1000
        steps_count = 0
        active_weights = np.nonzero(alpha)[1].tolist()
        self.active_atoms = self.active_atoms.union(set(active_weights))

        while True:
            converged = True
            for j in np.nonzero(alpha):
            #for j in range(self.dict_size):
                old = self.D[:, j].copy()
                u = (self.B[:, j] - np.dot(self.D, self.A[:, j])) / (self.A[j, j]+1e-15) + self.D[:, j]
                self.D[:, j] = u / max(np.linalg.norm(u), 1)

                steps_count += 1

                if (np.linalg.norm(self.D[:, j] - old) > self.bcd_epsilon) and (steps_count < max_steps):
                    #print np.linalg.norm(self.D[:, j] - old)
                    converged = False

            if converged:
                break

    def _init_from_data(self, data, dict_size=None):
        """
        Initializes the algorithm based on data first given to it.

        We don't want to put the burden of initialization of internal model on the user. Instead, we use
        the first chunk of data given to us to infer this information. This method should only be called
        once when the data is first given to us.
        :param data: data on which we learn
        :return: nothing
        """
        # Figure out all sizes
        self.dim = data.shape[0]     # Dimensionality of data

        # If number of atoms is not specified, use data dimensionality for for a complete basis
        self.dict_size = dict_size if dict_size else 50 #self.dim * 3

        # Create and initialize model state matrices
        self.A = np.zeros((self.dict_size, self.dict_size))
        self.B = np.zeros((self.dim, self.dict_size))

        # We initialize the dictionary to be a random matrix whose columns have unity norm
        self.D = np.random.random((self.dim, self.dict_size)) - 0.5    # Create random matrix
        self.D /= np.linalg.norm(self.D, axis=0)                       # Make columns have unity norm


if __name__ == "__main__":
    test_data = np.array([[1, 0, 1,   1],
                          [0, 1, 0.3, 0],
                          [0, 1, 0,   0.2],
                          [1, 0, 1,   1]])

    odl = odl()

    # for column in test_data.T:
    #     print column
    #     odl.process_new_data(column)

    # Go through all faces
    face_files = os.listdir('./yalefaces')
    face_files = [f for f in face_files if not f.endswith('.txt')]

    num_faces = len(face_files)
    num_faces = 30

    # orig_dims = (243, 320)
    # orig_dims = (24, 32)
    orig_dims = (9, 12)

    X = np.zeros((9*12, num_faces))

    for n in range(num_faces):
        filename = face_files[n]
        face = misc.imread('./yalefaces/'+filename)
        face = misc.imresize(face, 0.04, 'cubic')
        X[:, n] = face.flatten()

    mean = X.mean(axis=1)
    Xc = X - mean[:, None]

    # fig, axes = plt.subplots(10, 10, figsize=(12, 6),
    #                              subplot_kw={'xticks': [], 'yticks': []})
    #
    # for ax, n in zip(axes.flat, range(100)):
    #     d = np.reshape(Xc[:, n], orig_dims)
    #     cax = ax.imshow(d, cmap=plt.cm.gray, interpolation='nearest')
    #     # cax = plt.imshow(d, cmap=plt.cm.gray, interpolation='nearest')
    #     #cbar = plt.colorbar(cax, orientation='vertical')
    #
    # pylab.show()

    count = 0
    for epoch in range(100):
        for column in Xc.T:
            print count
            odl.process_new_data(column / 255.0)
            count += 1

    active_atoms = list(odl.active_atoms)
    print "Number of active atoms: ", len(active_atoms)
    D = odl.D
    d = np.reshape(D[:, 0], orig_dims)

    num_pages = len(active_atoms) // 100
    for page in range(num_pages):
        fig, axes = plt.subplots(10, 10, figsize=(12, 6),
                                 subplot_kw={'xticks': [], 'yticks': []})

        for ax, n in zip(axes.flat, active_atoms[page*100:(page+1)*100]):
            d = np.reshape(D[:, n], orig_dims)
            #cax = ax.imshow(d, cmap=plt.cm.gray, interpolation='nearest', vmin=-1, vmax=1)
            cax = ax.imshow(d, cmap=plt.cm.gray, interpolation='nearest')
            # cax = plt.imshow(d, cmap=plt.cm.gray, interpolation='nearest')
            #cbar = plt.colorbar(cax, orientation='vertical')

        pylab.show()

    fig, axes = plt.subplots(10, 10, figsize=(12, 6),
                                 subplot_kw={'xticks': [], 'yticks': []})

    for ax, n in zip(axes.flat, active_atoms[:len(active_atoms) - num_pages*100]):
        d = np.reshape(D[:, n], orig_
        dims)
        #cax = ax.imshow(d, cmap=plt.cm.gray, interpolation='nearest', vmin=-1, vmax=1)
        cax = ax.imshow(d, cmap=plt.cm.gray, interpolation='nearest')
        # cax = plt.imshow(d, cmap=plt.cm.gray, interpolation='nearest')
        #cbar = plt.colorbar(cax, orientation='vertical')

    pylab.show()

